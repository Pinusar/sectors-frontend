import {createRouter, createWebHashHistory} from 'vue-router'
import HomePage from './../components/HomePage.vue'
import SectorsForm from "@/components/SectorsForm.vue";


export default createRouter({
    history: createWebHashHistory(),
    routes: [
        { path: '/', component: HomePage, name: 'Home' },
        { path: '/sectorsForm', component: SectorsForm },
    ]
})